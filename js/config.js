var config = (function() {
	var config = function() { }
	var $controls = {
		pause: $('#config-controls-pause'),
		volume: $('#config-controls-volume'),
		speed: $('#config-controls-speed'),
		hide: $('#config-controls-hide'),
		fullscreen: $('#config-controls-fullscreen'),
	};
	var $cover = $('#config-cover');
	var $blur = $('#config-blur');

	$.extend(config, {
		data: {
			thumb: "./media/thumb.jpg",
		},
		loadControls: function(arr) {
			for (var c in $controls) {
				$controls[c].e.checked = (arr.indexOf(c) > -1);
			}
		},
		load: function(config) {
			this.data = config;
			this.loadControls(config.controls || []);
			$cover.e.checked = this.data.cover || false;
			$blur.e.checked = this.data.blur || false;
		},
		updateControls: function() {
			var arr = [];
			for (var c in $controls) {
				if (!$controls[c].e.checked) continue;
				arr.push(c);
			}
			this.data.controls = arr;

			this.update();
		},

		update: function() {
			this.data.cover = $cover.e.checked || undefined;
			this.data.blur = $blur.e.checked || undefined;

			json.data.config = this.data;
			json.update();
		},

		load: function(config) {
			config = config || {};

			config.controls = config.controls || [];
			$controls.pause.e.checked = (config.controls.indexOf('pause') != -1);
			$controls.volume.e.checked = (config.controls.indexOf('volume') != -1);
			$controls.speed.e.checked = (config.controls.indexOf('speed') != -1);
			$controls.hide.e.checked = (config.controls.indexOf('hide') != -1);
			$controls.fullscreen.e.checked = (config.controls.indexOf('fullscreen') != -1);

			$cover.e.checked = (!!config.cover);
			$blur.e.checked = (!!config.blur);
		},
	});

	for (var c in $controls) {
		$controls[c]
			.on('input', function() {
				config.updateControls();
			});
	}

	$cover.on('input', function() {
		config.update();
	})
	$blur.on('input', function() {
		config.update();
	})

	$.load(function() {
		config.updateControls();
		config.update();
	});

	return config;
})();
