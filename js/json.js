/**
 * Handles serializing/deserializing the JSON in the JSON viewer element.
 */
var json = (function() {
	var $viewer = $('#json-viewer');
	var $loader = $('#json-loader');

	var $loadButton = $('#json-load');
	var $copyButton = $('#json-copy');

	var json = function() { }

	$.extend(json, {
		v0: jsonv0(),
		v1: jsonv1(),

		data: {},

		load: function(data) {
			editor.clear();
			this.data = {};

			this.data = (function(data) {
				data = JSON.parse(data);
				if (!data.version) {
					console.warn("JSON does not have a version string!");
					return json.v0(data);
				}
				switch(data.version) {
					case 1:
						return json.v1(data);
					default:
						console.warn("Loading unsupported JSON version as v0: " + data.version + "");
						return json.v0(data);
				}
			})(data);

			console.log("Loaded JSON: ", this.data);

			scenes.load(this.data.scenes);
			config.load(this.data.config);

			this.update();
		},

		update: function() {
			$viewer.text(JSON.stringify(this.data));
		},
		loadFile: function() {
			$loader.click();
		},
		parse: function(str) {
			// TODO: Convert from JS "JSON" to proper JSON
			return JSON.parse(str);
		},
	});

	$loader.on('input', function() {
		var fr = new FileReader();
		fr.onloadend = function() {
			var jsonString = "";
			if (this.result.indexOf('Player.load') > -1) {
				var html = this.result.replace(/\n/g, '');
				var match = html.match(/Player\.load\(.*\)/);
				if (match == null) {
					return alert('Could not load JSON from HTML: Player.load not found');
				}
				var js = match[0];
				jsonString = js.replace(/^Player\.load\(|\)$/g, '');
			} else {
				jsonString = this.result;
			}

			json.load(jsonString);
		}

		fr.readAsText(this.e.files[0]);
	});

	$copyButton.on('click', function() {
		$viewer.text(JSON.stringify(json.data, null, 2));
		$viewer.e.select();
		$viewer.e.setSelectionRange(0, 1e6);

		document.execCommand('copy');
	});

	$loadButton.on('click', function() {
		json.loadFile();
	});

	return json;
})();
