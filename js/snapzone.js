var $Snapzone = (function() {
	var $Snapzone = function(media, callback) {
		this.callback = callback;
		this.stub = $('>div')
			.class('stub');
		this.element = $('>div')
			.class('snapzone')
			.append(
				this.stub,
			)

		var self = this;
		function shouldSnap() {
			// Do not snap when we are not dragging.
			if (!editor.dragging) return false;
			// Do not snap to already attached zones.
			if (!self.empty) return false;
			// Do not snap scenes, as they should not be children to any elements.
			if (editor.dragging.element.data('type') == 'scene') return false;
			return true;
		}

		this.element.on('mouseover', function() {
			if (!shouldSnap()) return;
			self.snap();
		});

		this.element.on('mouseleave', function() {
			if (!shouldSnap()) return;
			self.snapStop();
		});

		this.element.on('mouseup', function() {
			if (!shouldSnap()) return;
			self.snapFinish();
		});

		Object.defineProperty(this, 'empty', {
			get: function() {
				return this.stub.children().length == 0;
			},
		});

		if (media) {
			this.attach(media);
		}
	}

	$.extend($Snapzone.prototype, {
		_toAlt: function() {
			return this.element;
		},

		attach: function(media) {
			this.callback(media);
			media.attach(this);
			this.stub.append(media);
		},
		attachStop: function() {
			this.callback();
		},

		snap: function() {
			this.element.addClass('snapped');
		},
		snapFinish: function() {
			this.snapStop();
			this.attach(editor.dragging);
			scenes.update();
		},
		snapStop: function() {
			this.element.removeClass('snapped');
		},
	});

	return $Snapzone;
})();
