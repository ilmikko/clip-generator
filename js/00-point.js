var Point = (function() {
	var Point = function(x, y) {
		this.x = x;
		this.y = y;
	}

	$.extend(Point, {
		clone: function(p) {
			return this.fromPoint(p);
		},
		fromElement: function(el) {
			if (el.e) el = el.e;
			return new Point(el.offsetLeft, el.offsetTop);
		},
		fromEvent: function(evt) {
			return new Point(evt.pageX, evt.pageY);
		},
		fromPoint: function(p) {
			return new Point(p.x, p.y);
		},
	});

	return Point;
})()
