var $MediaAll = (function() {
	var nextId = (function() {
		var id = 0;
		return function() {
			return id++;
		}
	})();

	var $MediaAll = function(all) {
		var self = this;

		this.snapzones = [];
		this.list = $('>div').class('list');
		this.element = $('>div').append(
			this.list,
		);

		this.element = new $Media(this, "All").element;

		this.element.data({
			type: 'all',
		});

		this.expand();

		this.medias = [];
		for (var g = 0, gg = all.src.length; g < gg; g++) {
			this.snapzones[g].attach($Media.create(all.src[g]));
		}
	}

	$MediaAll.prototype = Object.create($Media.prototype);

	$.extend($MediaAll.prototype, {
		expand: function() {
			var self = this;
			var i = nextId();
			this.snapzones[i] = (function(i) {
				return new $Snapzone(null, function(media) {
					if (media) {
						self.medias[i] = media;
						self.expand();
					} else {
						this.element.remove();
						delete self.medias[i];
					}
				});
			})(i);
			this.list.append(this.snapzones[i]);
		},
		toData: function() {
			var self = this;
			var arr = [];
			for (var g in self.medias) {
				arr.push(self.medias[g]);
			}
			return {
				all: arr,
			};
		},
	});

	return $MediaAll;
})();
