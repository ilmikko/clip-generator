var $MediaVideo = (function() {
	var $MediaVideo = function(media) {
		var self = this;
		this.data = {
			video: media.src,
		};

		var video = $('>video')

		if (media.src) {
			video.set({ src: media.src });
		}

		var text = $('>p')
			.class('source');
		var button = $('>button')
			.class('imgchooser')
			.text('Load file...');

		var loader = $('>input')
			.set({
				accept: 'video/*',
				type: 'file',
			});

		button.on('click', function() {
			// Load a video.
			loader.click();
		});

		video.on('dblclick', function() {
			loader.click();
		});

		video.on('error', function() {
			self.element.addClass('error');
			text.text(self.data.video);
		});

		loader.on('input', function() {
			var fr = new FileReader();
			fr.onloadend = function() {
				video.e.src = this.result;
			}

			fr.readAsDataURL(this.e.files[0]);

			self.data.video = this.e.files[0].name;
			scenes.update();
		});

		this.element = $('>div')
			.append(
				video,
				button,
				text,
			);

		this.element = new $Media(this, "Video").element;

		this.element.data({
			type: 'video',
		});
	}

	$MediaVideo.prototype = Object.create($Media.prototype);
	return $MediaVideo;
})();
