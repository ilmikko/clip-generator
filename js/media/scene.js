var $MediaScene = (function() {
	var $MediaScene = function(media) {
		var self = this;

		this.id = media.id || scenes.newId();

		this.media = $Media.create(media.src);

		this.snapzone = new $Snapzone(this.media, function(media) {
			self.media = media;
		});

		var input = $('>input')
			.css({
				marginRight: '2px',
			})
			.set({
				type: 'text',
			})
			.on('input', function() {
				self.id = this.value()

				scenes.update();
			})
			.on('keydown', function(e) {
				if (e.keyCode == 13) { // ENTER
					this.blur();
				}
			});

		input.value(this.id);

		this.element = [
			$('>label').append(
				$('>span')
				.text('ID:'),
				input,
			),
			this.snapzone,
		];

		this.element = new $Media(this, "Scene").element;

		this.element
			.data({
				type: 'scene',
				id: this.id,
			});

		scenes.add(this);
	}

	$MediaScene.prototype = Object.create($Media.prototype);
	$.extend($MediaScene.prototype, {
		toData: function() {
			return this.media;
		},
		remove: function() {
			this.attachStop();
			this.element.remove();
			scenes.remove(this);
		},
	});

	return $MediaScene;
})();
