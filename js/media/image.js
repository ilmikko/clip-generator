var $MediaImage = (function() {
	var $MediaImage = function(media) {
		var self = this;
		this.data = {
			image: media.src,
		};

		var button = $('>button')
			.class('imgchooser')
			.text('Choose image...');
		var image = $('>img');
		var loader = $('>input')
			.set({
				accept: 'image/*',
				type: 'file',
			});

		button.on('click', function() {
			loader.click();
		});

		image.on('dblclick', function() {
			loader.click();
		});

		image.on('error', function() {
			self.element.addClass('error');
		});

		loader.on('input', function() {
			var fr = new FileReader();
			fr.onloadend = function() {
				image.e.src = this.result;
			}

			fr.readAsDataURL(this.e.files[0]);

			self.data.image = this.e.files[0].name;
			scenes.update();
		});

		this.element = [
			image,
			button,
		];

		this.element = new $Media(this, "Image").element;

		this.element.data({
			type: 'image',
		});
	}

	$MediaImage.prototype = Object.create($Media.prototype);
	return $MediaImage;
})();
