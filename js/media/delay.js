var $MediaDelay = (function() {
	var $MediaDelay = function(delay) {
		var self = this;
		this.data = {
			delay: delay.src,
		};

		var input = $('>input')
			.css({
				width: '40px',
				marginRight: '2px',
			})
			.set({
				type: 'text',
			})
			.on('input', function() {
				self.data.delay = parseFloat(this.value())

				scenes.update();
			})
			.on('keydown', function(e) {
				if (e.keyCode == 13) { // ENTER
					this.blur();
				}
			})

		input.value(this.data.delay);

		this.element = $('>div')
			.css({
				alignItems: 'baseline',
			})
			.append(
				input,
				$('>span')
				.text('sec')
			);

		this.element = new $Media(this, "Delay").element;

		this.element.data({
			type: 'delay',
		});
	}

	$MediaDelay.prototype = Object.create($Media.prototype);
	return $MediaDelay;
})();
