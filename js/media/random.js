var $MediaRandom = (function() {
	var nextId = (function() {
		var id = 0;
		return function() {
			return id++;
		}
	})();

	var $MediaRandom = function(random) {
		var self = this;

		this.snapzones = [];
		this.list = $('>div').class('list');
		this.element = $('>div').append(
			this.list,
		);

		this.element = new $Media(this, "Random").element;

		this.element.data({
			type: 'random',
		});

		this.expand();

		this.medias = [];
		for (var g = 0, gg = random.src.length; g < gg; g++) {
			this.snapzones[g].attach($Media.create(random.src[g]));
		}
	}

	$MediaRandom.prototype = Object.create($Media.prototype);

	$.extend($MediaRandom.prototype, {
		expand: function() {
			var self = this;
			var i = nextId();
			this.snapzones[i] = (function(i) {
				return new $Snapzone(null, function(media) {
					if (media) {
						self.medias[i] = media;
						self.expand();
					} else {
						this.element.remove();
						delete self.medias[i];
					}
				});
			})(i);

			this.list.append(this.snapzones[i]);
		},
		toData: function() {
			var self = this;
			var arr = [];
			for (var g in self.medias) {
				arr.push(self.medias[g]);
			}
			return {
				random: arr,
			};
		},
	});

	return $MediaRandom;
})();
