var $MediaGoto = (function() {
	var $MediaGoto = function(_goto) {
		var self = this;
		this.goto = _goto.src;

		var input = $('>input')
			.set({
				type: 'text',
				value: this.goto,
			})
			.on('keydown', function(e) {
				if (e.keyCode == 13) { // ENTER
					this.blur();
				}
			})

		this.element = $('>label').append(
			$('>span').text('Scene: '),
			input
			.css({
				width: '120px',
			})
			.on('focus', function() {
				editor.nodeSelect(function(node) {
					if (!(node.element.data('type') == 'scene')) return;
					input.value(self.goto = node.element.data('id'));

					scenes.update();
				});
			})
			.on('input', function() {
				self.goto = this.value();

				scenes.update();
			})
		);

		this.element = new $Media(this, "Goto").element;

		this.element.data({
			type: 'goto',
		});
	}

	$MediaGoto.prototype = Object.create($Media.prototype);

	$.extend($MediaGoto.prototype, {
		toData: function() {
			return {
				goto: this.goto,
			};
		},
	});

	return $MediaGoto;
})();
