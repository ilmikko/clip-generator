var $MediaLoop = (function() {
	var $MediaLoop = function(loop) {
		var self = this;

		this.media = $Media.create(loop.src);

		this.element = new $Snapzone(this.media, function(media) {
			self.media = media;
		});

		this.element = new $Media(this, "Loop").element;

		this.element.data({
			type: 'loop',
		});
	}

	$MediaLoop.prototype = Object.create($Media.prototype);

	$.extend($MediaLoop.prototype, {
		toData: function() {
			return {
				loop: this.media,
			};
		},
	});

	return $MediaLoop;
})();
