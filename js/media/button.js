var $MediaButton = (function() {
	var $MediaButton = function(button) {
		var self = this;
		this.data = {
			text: button.src,
		};

		var input = $('>input')
			.set({
				type: 'text',
			});

		input.value(this.data.text);

		this.element = $('>label').append(
			$('>span').text('Text: '),
			input
			.css({
				width: '120px',
			})
			.on('input', function() {
				self.data.text = this.value();

				scenes.update();
			})
		);

		this.element = new $Media(this, "Button").element;

		this.element.data({
			type: 'button',
		});
	}

	$MediaButton.prototype = Object.create($Media.prototype);

	$.extend($MediaButton.prototype, {
		toData: function() {
			return {
				button: this.data.text,
			};
		},
	});

	return $MediaButton;
})();
