var $MediaAudio = (function() {
	var $MediaAudio = function(media) {
		var self = this;
		this.data = {
			audio: media.src,
		};

		var audio = $('>button')
			.text('Load file...');
		var loader = $('>input')
			.set({
				accept: 'audio/*',
				type: 'file',
			});

		audio.on('click', function() {
			loader.click();
		});

		audio.on('error', function() {
			self.element.addClass('error');
		});

		loader.on('input', function() {
			var name = this.e.files[0].name;
			audio.text(name);
			self.data.audio = name;
			scenes.update();
		});

		this.element = $('>div')
			.append(
				audio,
			);

		this.element = new $Media(this, "Audio").element;

		this.element.data({
			type: 'audio',
		});
	}

	$MediaAudio.prototype = Object.create($Media.prototype);
	return $MediaAudio;
})();
