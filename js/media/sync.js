var $MediaSync = (function() {
	var $MediaSync = function(sync) {
		var self = this;

		this.sync = $Media.create(sync.src);
		this.with = $Media.create(sync.with);

		this.element = $('>div').append(
			new $Snapzone(this.sync, function(media) {
				self.sync = media;
			}),
			new $Snapzone(this.with, function(media) {
				self.with = media;
			})
		);

		this.element = new $Media(this, "Sync").element;

		this.element.data({
			type: 'sync',
		});
	}

	$MediaSync.prototype = Object.create($Media.prototype);

	$.extend($MediaSync.prototype, {
		toData: function() {
			return {
				sync: this.sync,
				with: this.with,
			};
		},
	});

	return $MediaSync;
})();
