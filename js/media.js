var $Media = (function() {
	var $Media = function(parent, title) {
		var self = this;
		this.data = undefined;

		this.element = $('>div')
			.class('media');

		this.element
			.append(
				$('>div').class('title')
				.append(
					$('>span').text(title)
					.on('click', function() {
						self.element.toggleClass('minimized');
					}),
					new $Dragger(parent),
				),
				$('>div').class('content')
				.append(
					parent.element,
				),
			);

		this.element
			.on('click', function(evt) {
				evt.stopPropagation();
			})
			.on('mousedown', function(evt) {
				evt.stopPropagation();
				editor.select(parent);
			})
			.on('mouseup', function(evt) {
				evt.stopPropagation();
				editor.dragStop();
			});
	}

	$.extend($Media, {
		create: function(media) {
			if (!media) return;
			if (!media.type) throw new Error('No type in media!');

			var types = {
				queue: $MediaQueue,
				loop: $MediaLoop,
				random: $MediaRandom,
				goto: $MediaGoto,
				video: $MediaVideo,
				audio: $MediaAudio,
				image: $MediaImage,
				button: $MediaButton,
				delay: $MediaDelay,
				all: $MediaAll,
				sync: $MediaSync,
			};
			return new types[media.type](media);
		},
	});

	$.extend($Media.prototype, {
		_toAlt: function() {
			return this.element;
		},
		toData: function() {
			return this.data;
		},

		attach: function(zone) {
			if (this.attachzone) return;

			this.attachzone = zone;

			this.element
				.addClass('attached')
				.css({
					left: 0,
					top: 0,
				});
		},
		attachStop: function() {
			if (!this.attachzone) return;

			this.attachzone.attachStop();
			this.attachzone = null;

			var point = Point.fromPoint(editor.mouse);

			point.x -= editor.element.e.offsetLeft;
			point.y -= editor.element.e.offsetTop;

			point.x -= editor.width / 2;
			point.y -= editor.height / 2;

			this.element.removeClass('attached');

			editor.add(this, point);
		},

		drag: function() {
			this.element.addClass('dragged');
		},
		dragStop: function() {
			this.element.removeClass('dragged');
		},

		remove: function() {
			this.attachStop();
			this.element.remove();
		},

		setPos: function(point) {
			var w = editor.width, h = editor.height;
			var sw = this.element.e.offsetWidth, sh = this.element.e.offsetHeight;

			this.element.css({
				left: (w / 2) - (sw / 2) + point.x + 'px',
				top: (h / 2) - (sh / 2) + point.y + 'px',
			});
		},
		setIndex: function(index) {
			this.element.css({ zIndex: index });
		},

		select: function() {
			this.element.addClass('selected');
		},
		selectStop: function() {
			this.element.removeClass('selected');
		},
	});

	return $Media;
})();
