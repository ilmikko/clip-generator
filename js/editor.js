window.editor = (function() {
	var $editor = $('#editor');

	var nextIndex = (function() {
		var i = 0;
		return function() {
			return ++i;
		}
	})();

	var editor = function() {}

	$.extend(editor, {
		element: $editor,
		get width() {
			return $editor.e.offsetWidth;
		},
		get height() {
			return $editor.e.offsetHeight;
		},
		add: function(media, point) {
			$editor.append(media);
			media.setPos(point || new Point(0, 0));
			media.setIndex(nextIndex());

			scenes.update();
		},

		clear: function() {
			scenes.clear();
			$editor.clear();
		},

		click: function(point) {
			// Convert to centered coordinates
			point.x -= $editor.e.offsetLeft + this.width / 2;
			point.y -= $editor.e.offsetTop + this.height / 2;

			tools.createMedia(point);
		},

		delete: function() {
			if (!this.selected) return;
			this.selected.remove();
			this.selectStop();

			scenes.update();
		},

		drag: function(media, point) {
			// When dragged, we want to detach the media.
			media.attachStop();

			this.dragging = media;
			this.draggingStart = point;

			this.draggingPos = Point.fromElement(media.element);
			this.draggingPos.x -= this.width / 2;
			this.draggingPos.y -= this.height / 2;
			this.draggingPos.x += media.element.e.offsetWidth / 2;
			this.draggingPos.y += media.element.e.offsetHeight / 2;

			media.drag();
		},
		dragStop: function() {
			if (this.dragging)
				this.dragging.dragStop();
			this.dragging = this.draggingStart = null;
		},

		move: function(point) {
			this.mouse = point;

			if (this.dragging) {
				var dx = point.x - this.draggingStart.x;
				var dy = point.y - this.draggingStart.y;

				var x = this.draggingPos.x + dx;
				var y = this.draggingPos.y + dy;

				this.dragging.setPos(new Point(x, y));
			}
		},

		nodeSelect: function(callback) {
			this.onselect = callback;
		},
		nodeSelectStop: function() {
			if (this.onselect) {
				if (this.selected)
					this.onselect(this.selected);
				this.onselect = null;
			}
		},

		select: function(media) {
			this.selectStop();

			this.selected = media;
			this.selected.setIndex(nextIndex());
			this.selected.select();

			this.nodeSelectStop();
		},
		selectStop: function() {
			if (this.selected)
				this.selected.selectStop();
			this.selected = null;
		},
	});

	$editor
		.on('mousemove', function(evt) {
			editor.move(
				Point.fromEvent(evt)
			);
		})
		.on('mouseleave', function(evt) {
			editor.dragStop();
		})
		.on('mouseup', function(evt) {
			if (editor.dragging) { editor.dragStop(); return; }
			if (editor.selected) { editor.selectStop(); editor.nodeSelectStop(); return; }

			editor.click(
				Point.fromEvent(evt)
			);
		})

	$(document)
		.on('keydown', function(evt) {
			if (evt.keyCode == 46) { // DEL
				editor.delete();
			}
		});

	return editor;
})();
