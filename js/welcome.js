window.welcome = (function() {
	var $newButton = $('#welcome-new');
	var $loadButton = $('#welcome-load');
	var $closeButton = $('#welcome-close');
	var $greetOverlay = $('#greet-overlay');

	var welcome = {
		close: function() {
			$greetOverlay.css({
				display: 'none',
			});
		},
	};

	$closeButton.on('click', function() {
		welcome.close();

		// Start with one main scene.
		scenes.create({id: 'main'});
	});

	$loadButton.on('click', function() {
		welcome.close();

		json.loadFile();
	});

	$newButton.on('click', function() {
		welcome.close();

		// Start with one main scene.
		scenes.create({id: 'main'});
	});

	return welcome;
})();
