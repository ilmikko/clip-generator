var scenes = (function() {
	var scenes = function() { }

	var nextId = (function() {
		var id = 0;
		return function nextId() {
			return "scene-" + ++id;
		}
	})();

	$.extend(scenes, {
		data: {},
		add: function(scene) {
			this.data[scene.id] = scene;
			this.update();
		},

		clear: function() {
			this.data = {};
		},

		create: function(media) {
			var scene = new $MediaScene(media);
			editor.add(scene);
			scenes.add(scene);
		},

		getData: function(data) {
			function getData_r(data) {
				while (data && data.toData) {
					data = data.toData();
				}

				if (typeof data !== 'object') return data;
				if (data instanceof Array) {
					var odata = [];
					for (var g = 0, gg = data.length; g < gg; g++) {
						odata.push(getData_r(data[g]));
					}
					return odata;
				}

				var odata = {};
				for (var g in data) odata[g] = getData_r(data[g]);

				return odata;
			}
			return getData_r(data);
		},
		newId: function() {
			return nextId();
		},
		remove: function(scene) {
			delete this.data[scene.id];
			this.update();
		},
		update: function() {
			json.data.scenes = {};
			for (var g in this.data) {
				json.data.scenes[this.data[g].id] = this.getData(this.data[g]);
			}
			json.update();
		},

		load: function(_scenes) {
			for (var id in _scenes) {
				var scene = {
					id: id,
					src: _scenes[id],
				};
				scenes.create(scene);
			}
		},
	});

	return scenes;
})();
