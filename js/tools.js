var tools = (function() {
	var tools = function() { }

	var $$tools = $$('input[name=tool]');

	var media = {};

	$.extend(tools, {
		createMedia: function(point) {
			var type = this.current;

			if (!media[type]) {
				throw new Error("Unknown media type: " + type);
			}

			editor.add(new media[type]({}), point);
		},
		use: function(tool) {
			this.current = tool;
		},
	});

	$$tools.on('input', function() {
		tools.use(this.value());
	});

	$.load(function() {
		tools.use('scene');

		media = {
			all:    $MediaAll,
			audio:  $MediaAudio,
			button: $MediaButton,
			delay:  $MediaDelay,
			goto:   $MediaGoto,
			image:  $MediaImage,
			loop:   $MediaLoop,
			queue:  $MediaQueue,
			random: $MediaRandom,
			scene:  $MediaScene,
			sync:   $MediaSync,
			video:  $MediaVideo,
		};
	});

	return tools;
})();
