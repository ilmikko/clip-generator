var $Dragger = (function() {
	var $Dragger = function(parent) {
		this.element = $('>div')
			.class('dragger');

		this.element.on('mousedown', function(evt) {
			editor.drag(parent,
				Point.fromEvent(evt)
			);
		});
	}

	$.extend($Dragger.prototype, {
		_toAlt: function() {
			return this.element;
		},
	});

	return $Dragger;
})();
