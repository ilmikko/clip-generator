var jsonv1 = (function() {
	var elements = [
		'all',
		'audio',
		'button',
		'delay',
		'goto',
		'image',
		'loop',
		'queue',
		'random',
		'sync',
		'video',
	];

	function parseImplicitTypes(o) {
		// Inherited from Clipci expand function

		// Leaves
		if (typeof o !== 'object') return o;

		// Arrays
		if (o.length && !isNaN(o.length)) {
			for (var g = 0, gg = o.length; g < gg; g++) {
				o[g] = parseImplicitTypes(o[g]);
			}
			return o;
		}

		if (!o.type) {
			for (var g in elements) {
				if (elements[g] in o) {
					o.type = elements[g];
					o.src = o[o.type];
					delete o[o.type];
					break;
				}
			}
		}

		// Recurse to leaves
		for (var g in o) {
			o[g] = parseImplicitTypes(o[g]);
		}

		return o;
	}

	function jsonv1() {
		return function(json) {
			json = parseImplicitTypes(json);
			return json;
		}
	}

	return jsonv1;
})();
