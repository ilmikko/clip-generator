var jsonv0 = (function() {
	function jsonv0() {
		return function(json) {
			console.warn("Migration from v0 is still unstable.");
			json = jsonv1()(json);
			return json;
		}
	}

	return jsonv0;
})();
