#!/usr/bin/env bash

DIR="$(dirname "$0")";
JS_DIR="$DIR/js";
CSS_DIR="$DIR/css";
HTML_DIR="$DIR/html";

compile-css() {
echo "Compiling css...";
cat "$CSS_DIR/"*"css" > "$dir/app.css";
}

compile-js() {
echo "Compiling JS...";
cat "$JS_DIR/json/"*"js" > "$dir/app.js";
cat "$JS_DIR/"*"js" >> "$dir/app.js";
cat "$JS_DIR/media/"*"js" >> "$dir/app.js";
}

compile-html() {
echo "Compiling html...";
cat "$HTML_DIR/index.template.html" > "$dir/index.html";
}

compile() {
	dir="$DIR/compiled";
	mkdir -p "$dir";
	compile-js "$dir";
	compile-css "$dir";
	compile-html "$dir";
	cp -rv "projects" "$dir";
	echo "Creator compiled to $dir.";
}

compile "$@";
